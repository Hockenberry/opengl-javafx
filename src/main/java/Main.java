import com.jogamp.newt.awt.NewtCanvasAWT;
import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import com.sun.awt.AWTUtilities;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.Background;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Main {

    private final NewtCanvasAWT glcanvas;

    private final static class WindowUtils {
        /**
         * Return the default graphics configuration.
         */
        public static GraphicsConfiguration getAlphaCompatibleGraphicsConfiguration() {
            GraphicsEnvironment env = GraphicsEnvironment
                    .getLocalGraphicsEnvironment();
            GraphicsDevice dev = env.getDefaultScreenDevice();
            return dev.getDefaultConfiguration();
        }
    }

    public Main() {

        GLEventListener eventListener = new GLEventListener() {
            private OneTriangle triangle = new OneTriangle();

            @Override
            public void reshape(GLAutoDrawable glautodrawable, int x, int y, int width, int height) {
                triangle.setup(glautodrawable.getGL().getGL2(), width, height);
            }

            @Override
            public void init(GLAutoDrawable glautodrawable) {
                triangle.render(glautodrawable.getGL().getGL2(), glautodrawable.getSurfaceWidth(), glautodrawable.getSurfaceHeight());
            }

            @Override
            public void dispose(GLAutoDrawable glautodrawable) {
            }

            @Override
            public void display(GLAutoDrawable glautodrawable) {
                triangle.render(glautodrawable.getGL().getGL2(), glautodrawable.getSurfaceWidth(), glautodrawable.getSurfaceHeight());
            }
        };

        glcanvas = setupOpenGL2(eventListener);


        // JFrame frame = new JFrame("OpenGL-JavaFX Main", WindowUtils.getAlphaCompatibleGraphicsConfiguration());
        JFrame frame = new JFrame("OpenGL-JavaFX Main");


        frame.setSize(720, 567);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                frame.remove(glcanvas);
                frame.dispose();
                System.exit(0);
            }
        });

        frame.setVisible(true);
        frame.add(initAndShowGUI());
        frame.add(glcanvas);
        frame.pack();


//        SwingUtilities.invokeLater(() -> {
//            VideoPlayer videoPlayer = new VideoPlayer();
//            videoPlayer.setVideoSurface(glcanvas);
//            videoPlayer.setOverlay(new Overlay(frame));
//            videoPlayer.enableOverlay(true);
//        });
    }

    public static void main(String[] args) {
        new Main();
    }

    private GLCanvas setupOpenGL() {
        GLProfile glprofile = GLProfile.getDefault();
        GLCapabilities glcapabilities = new GLCapabilities(glprofile);
        return new GLCanvas(glcapabilities);
    }

    private NewtCanvasAWT setupOpenGL2(GLEventListener eventListener) {
        GLProfile glprofile = GLProfile.getDefault();
        GLCapabilities glcapabilities = new GLCapabilities(glprofile);

        GLWindow window = GLWindow.create(glcapabilities);

        window.addGLEventListener(eventListener);

        NewtCanvasAWT canvas = new NewtCanvasAWT(window);
        canvas.setShallUseOffscreenLayer(true);

        return canvas;
    }


    private JFXPanel innerScenePane;
    private ComboBox<String> selectLan;

    private JPanel initAndShowGUI() {
        JPanel outerPane = new TransparentPanel();
        outerPane.setBounds(0, 0, 200, 400);
        outerPane.setLayout(null);
        outerPane.setBackground(new java.awt.Color(0, 0, 255, 0));

        innerScenePane = new JFXPanel();
        innerScenePane.setBounds(35, 200, 100, 100);
        outerPane.setBackground(new java.awt.Color(0, 0, 255, 0));
        outerPane.add(innerScenePane);

        outerPane.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Platform.exit();
                System.exit(0);
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });


        Platform.runLater(() -> initFX());

        return outerPane;
    }

    private void initFX() {
        // This method is invoked on the JavaFX thread
        Scene scene = initSelectLan();
        scene.setFill(Color.TRANSPARENT);
        innerScenePane.setScene(scene);
    }

    private Scene initSelectLan() {
        StackPane glass = new StackPane();
        glass.setBackground(Background.EMPTY);
        selectLan = new ComboBox<>();
        // glass.getChildren().addAll(selectLan);
        glass.setStyle("-fx-background: transparent; -fx-background-color: #00000000;");
        glass.setOpacity(1.0);
        selectLan.setStyle("-fx-background: transparent; -fx-background-color: #00000000;");
        selectLan.setOpacity(0.5);
        return new Scene(selectLan, Color.TRANSPARENT);
    }

    private class OneTriangle {
        protected void setup(GL2 gl2, int width, int height) {
            gl2.glMatrixMode(GL2.GL_PROJECTION);
            gl2.glLoadIdentity();

            // coordinate system origin at lower left with width and height same as the window
            GLU glu = new GLU();
            glu.gluOrtho2D(0.0f, width, 0.0f, height);

            gl2.glMatrixMode(GL2.GL_MODELVIEW);
            gl2.glLoadIdentity();

            gl2.glViewport(0, 0, width, height);
        }

        protected void render(GL2 gl2, int width, int height) {
            gl2.glClear(GL.GL_COLOR_BUFFER_BIT);

            // draw a triangle filling the window
            gl2.glLoadIdentity();
            gl2.glBegin(GL.GL_TRIANGLES);
            gl2.glColor3f(1, 0, 0);
            gl2.glVertex2f(0, 0);
            gl2.glColor3f(0, 1, 0);
            gl2.glVertex2f(width, 0);
            gl2.glColor3f(0, 0, 1);
            gl2.glVertex2f(width / 2, height);
            gl2.glEnd();
        }
    }

    private class Overlay extends Window {

        private static final long serialVersionUID = 1L;

        public Overlay(Window owner) {
            super(owner, WindowUtils.getAlphaCompatibleGraphicsConfiguration());
            // setLayout(null);

            AWTUtilities.setWindowOpaque(this, false);

            // JFXPanel panel = new JFXPanel();

            JButton b = new JButton("JButton");
            b.setBounds(150, 150, 100, 24);
            add(b);

            TranslucentComponent c = new TranslucentComponent();
            c.setBounds(150, 200, 300, 40);
            add(c);
        }

        @Override
        public void paint(Graphics g) {
            super.paint(g);

            Graphics2D g2 = (Graphics2D) g;
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

            GradientPaint gp = new GradientPaint(180.0f, 280.0f, new java.awt.Color(255, 255, 255, 255), 250.0f, 380.0f, new java.awt.Color(255, 255, 0, 0));
            g2.setPaint(gp);
            for (int i = 0; i < 3; i++) {
                g2.drawOval(150, 280, 100, 100);
                g2.fillOval(150, 280, 100, 100);
                g2.translate(120, 20);
            }
        }
    }


    private class TranslucentComponent extends JComponent {

        private static final long serialVersionUID = 1L;

        public TranslucentComponent() {
            setOpaque(false);
        }

        @Override
        protected void paintComponent(Graphics g) {
            Graphics2D g2 = (Graphics2D) g;

            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);

            g2.setPaint(new java.awt.Color(255, 128, 128, 64));

            g2.drawRect(0, 0, getWidth() - 1, getHeight() - 1);
            g2.fillRect(0, 0, getWidth(), getHeight());

            g2.setPaint(new java.awt.Color(0, 0, 0, 128));
            g2.setFont(new Font("Sansserif", Font.BOLD, 18));
            g2.drawString("Translucent", 16, 26);
        }
    }

}
